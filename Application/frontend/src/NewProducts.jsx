import React, { useState, useEffect } from "react";
import { Flex, Box, Text } from "@chakra-ui/react";
import axios from "axios";
import Popup from "./Popup";

function App() { 
    const [loading, setLoading] = useState(true);
    const [products, setProducts] = useState("");
    const [error, setError] = useState("");
    const [LargePic, SetLargePic] = useState(false);
      

const FetchProducts = async () => {
    setLoading(true);
    try {
      const response = await axios.get(`https://api.manoapp.com/api/v1/users/products/whats_new`, {
        headers: { "StoreID": "4", "Authorization": "f44a4aabfc5992514d262d7f517327e7", "UserAddressID": "60877",},
      });
      if (response) {
        setProducts(response.data.data.items);
        setLoading(false);
      }
    } catch (e) {
      setError(e);
      console.error(e.message);
    }
  }
  useEffect(() => {
    FetchProducts();
  }, []); 


  console.log({products})
  return(
    <div className="products-container">
          {!loading ? 
        products.map((item) => (
            <Flex
            alignItems="center"
            color="black"
          >
               
    <div className="box"  onClick={() => {
    
    SetLargePic(true);
    }}>

      <Box>
        <Box>
          <img
           alignItems="center"
         
         src={item.images[0].original}
            alt="Logo"
          />
        </Box>
        <Flex
          alignItems="center"
          color="blue"
        >
          Title : {item.title},
  
        </Flex>
        <Box >
          <Flex
            alignItems="center"
            color="black"
          >
            <Text fontWeight="bold" fontSize="lg">
                Price:
              {item.price}L.L
        
            </Text>
           
          </Flex>

        </Box>
      </Box>
    </div>
    <Popup trigger={LargePic} setTrigger={LargePic}>
    <img
           alignItems="center"
         
         src={item.images[0].large}
            alt="Logo"
          />
        
    
              <button

                onClick={() => {
    
                SetLargePic(false);
                }}
              >
                Done
              </button>
            </Popup>

    </Flex>
     ))
     : "Loading..."}
  </div>



  )
}
export default App;
